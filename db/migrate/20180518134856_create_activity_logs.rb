class CreateActivityLogs < ActiveRecord::Migration
  def change
    drop_table :activity_logs
    create_table :activity_logs do |t|
      t.integer :baby_id
      t.integer :assistant_id
      t.integer :activity_id
      t.datetime :start_time
      t.datetime :stop_time
      t.decimal :duration
      t.string :comments
      t.timestamps null: false
    end
  end
end