class CreateUsers < ActiveRecord::Migration
  def self.up
  	create_table :users do |t|
      t.boolean  :is_active, :default => true
      t.text     :comments
      t.string   :login, :limit => 32
      t.string   :email
      t.string   :language, :limit => 5
      t.string   :crypted_password
      t.string   :password_salt
      t.string   :persistence_token
      t.string   :single_access_token
      t.integer  :login_count, :default => 0
      t.integer  :failed_login_count, :default => 0
      t.datetime :last_request_at
      t.datetime :current_login_at
      t.datetime :last_login_at
      t.string   :current_login_ip
      t.string   :last_login_ip      
      t.string   :session_key
      t.integer  :created_by
      t.integer  :updated_by
      t.string   :perishable_token, :default => ""
      t.boolean  :verified, :default => false
      t.timestamps
    end

    #User Default - Administrator
    User.create(:login => "admin", :language => "es", :email => "demo@kinedu.com")
  end

  def self.down
  	drop_table :users
  end
end