class CreateRoles < ActiveRecord::Migration
  def self.up
    create_table :roles do |t|
      t.string  :title
      t.integer :user_id
      t.boolean :is_active, :default => false
      t.timestamps null: false
    end

    #Create Role Default
  	Role.create(:title => "activity_logs", :user_id => 1, :is_active => true) 
  end

  def self.down
  	drop_table :roles
  end
end