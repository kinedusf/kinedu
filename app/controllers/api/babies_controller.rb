module Api
  class BabiesController < ApplicationController

    def index 
      hash = Baby.all.map{|baby| [:name => baby.name, :age => "#{((Date.today.year - baby.birthday.to_date.year) * 12)} months", :parents => "#{baby.mother_name}, #{baby.father_name}", :contact => "Address: #{baby.address}, Phone: #{baby.phone}"]}.flatten
      render :json => hash.to_json
    end
    
  end
end