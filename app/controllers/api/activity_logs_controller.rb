module Api
  class ActivityLogsController < ApplicationController

    def index
      baby = Baby.find_by_id(params[:baby_id])
      if baby.present?
        hash = baby.activity_logs.map{|activity| [:activity_id => activity.id, :baby_id => activity.baby_id, :start_time => l(activity.start_time.to_datetime, :format => :short), :stop_time => (activity.stop_time.present? ? l(activity.stop_time.to_datetime, :format => :short) : "--"), :status => (activity.stop_time.present? ? "Finished" : "In Progress")]}.flatten
      else  
        hash = {:message => "Baby Not Found"}
      end
      render :json => hash.to_json
    end

    def create
      hash = {}
	    baby_id = params[:baby_id]
	    assistant_id = params[:assistant_id]
	    activity_id = params[:activity_id]
	    start_time = DateTime.now

	    if baby_id.present? && assistant_id.present? && activity_id.present?
	      activity_log = ActivityLog.new({:baby_id => baby_id, :assistant_id => assistant_id, :activity_id => activity_id, :start_time => start_time})
	      if activity_log.save
	        hash = {:success => true, :message => "Activity Saved"}
	      else
	        hash = {:success => false, :message => activity_log.errors.full_messages}
	      end
	    else
	      hash = {:success => false, :message => "Activity Not Saved, Blank Parameters"}
	    end
	    render :json => hash.to_json
    end

    def update
      hash = {}
      id = params[:id]
      activity_log = ActivityLog.find_by_id(id)

      if activity_log.update_attributes(:stop_time => DateTime.now, :duration => "#{(Time.now - activity_log.start_time) / 60}".to_i)
        hash = {:success => true, :message => "Activity Updated"}
      else  
        hash = {:success => false, :message => activity_log.errors.full_messages}
      end
      render :json => hash.to_json
    end

  end
end