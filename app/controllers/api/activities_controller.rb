module Api
  class ActivitiesController < ApplicationController
    
    def index 
      hash = Activity.all.map{|activity| [:id => activity.id, :name => activity.name, :description => activity.description]}.flatten
      render :json => hash.to_json
    end

  end
end