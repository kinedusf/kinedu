class ActivityLogsController < InheritedResources::Base

  has_scope :baby_id
  has_scope :assistant_id
  has_scope :activity_id

  def index
    @babies = Baby.all.map{|baby| [baby.name, baby.id]}
    @assistant = Assistant.all.map{|assistant| [assistant.name, assistant.id]}
    @activities = Activity.all.map{|activity| [activity.name, activity.id]}
    index!
    @activity_logs = apply_scopes(ActivityLog)
  end
  
end