class UserSessionsController < ApplicationController
  
  protect_from_forgery :only => [:edit, :update, :destroy] 
    
  skip_filter :require_authentification
  skip_filter :set_user_language, :only => [:new, :create, :destroy]
  
  def new
    unless current_user
      @user_session = UserSession.new
      flash[:notice] = "Bienvenido al Kinedu."
    else
      @user = current_user
      flash.clear
      redirect_to activity_logs_path
    end
  end
  
  def create
    @user_session = UserSession.new(params[:user_session])
    @user = User.find_by_login(params[:user_session][:login])
    if @user_session.save
      Authorization.current_user = @user
      flash[:notice] = t(:success_login)
      redirect_to activity_logs_path
    else
      flash[:warning] = @user_session.errors.full_messages.join(",")
      render :new
    end
  end
  
  def destroy
    @user_session = UserSession.find(params[:id])
    @user_session.destroy
    redirect_to root_path
  end

end
