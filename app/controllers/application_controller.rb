class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception

  helper_method :current_user, :current_user_session
  before_filter :set_user_language
  before_filter :set_current_user

  protect_from_forgery
  filter_access_to :all
  
  def permission_denied
    logger.info "** Permision Denied ** "
    set_user_language
    flash[:error] = t("priviledge_error")
    respond_to do |format|
      format.html { redirect_to(:back) rescue redirect_to root_path }
      format.xml  { head :unauthorized }
      format.js   { head :unauthorized }
    end
  end

  protected

    def clear_authlogic_session
      sess = current_user_session
      sess.destroy if sess
    end

  private
    
    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session ||= UserSession.find
    end
    
    def current_user
      if @current_user && @current_user.session_key && @current_user.session_key !=  request.session_options[:id]
        if @current_user.last_request_at
           flash[:error] = t("simultaneous_logins")
           @current_user.session_key = nil
           @current_user.save
           current_user_session.destroy
        else
          @current_user.session_key =  request.session_options[:id]
        end
      end
      return @current_user if defined?(@current_user)

      @current_user = current_user_session && current_user_session.record
      if @current_user
        logger.info "@current_user = " + @current_user.id.to_s
      else
        logger.info "@current_user not exists"
      end
      @current_user
    end

    def set_current_user
      if current_user
        Authorization.current_user = current_user
        Thread.current['user'] = current_user
      else
        Authorization.current_user = nil
        Thread.current['user'] = nil
      end
    end

    def set_user_language
      if current_user
        I18n.locale = current_user.language
      else
        I18n.locale = "es"
      end
    end
  
end