require "date"
require "digest"
require "digest/sha1"
require "authlogic"

class User < ActiveRecord::Base

  acts_as_authentic do |c|
    c.logged_in_timeout = 60.minutes
    c.crypto_provider = Authlogic::CryptoProviders::BCrypt
  end

  has_many   :roles

  default_value_for :language, "es"
  default_value_for :verified, false
  default_value_for :perishable_token, nil
  default_value_for :password, "nimda"
  default_value_for :password_confirmation, "nimda"

  def role_symbols
    (roles || []).map {|r| r.title.to_sym}
  end
    
end