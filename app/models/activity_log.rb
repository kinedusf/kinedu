class ActivityLog < ActiveRecord::Base

  belongs_to :baby
  belongs_to :assistant
  belongs_to :activity

  scope :baby_id, -> (baby_id){ where(:baby_id => baby_id)}
  scope :assistant_id, -> (assistant_id){ where(:assistant_id => assistant_id)}
  scope :activity_id, -> (activity_id){ where(:activity_id => activity_id)}
  
end
