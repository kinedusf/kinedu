module ErrorMessagesHelper
  
  #Form Errors
  def errorForm(record)
    if !record.errors.empty?
      message = content_tag(:h3, (t(:problems_found) + ": " + record.errors.count.to_s).html_safe)
      error_list = "".html_safe
      record.errors.full_messages.each do |msg|
        error_list += content_tag(:li, msg)
      end
      message += content_tag(:ul, error_list)
      message_frame = content_tag(:div, message, :class=>"flash error")
    end
  end
  
  #Flash Errors
  def errorFlash(flash)
    message_frame = "".html_safe
    if flash[:error]
      message_frame += content_tag(:div, flash[:error].html_safe, :class=>"flash error")
    end
    if flash[:warning]
      message_frame += content_tag(:div, flash[:warning].html_safe, :class=>"flash warning")
    end
    if flash[:notice]
      message_frame += content_tag(:div, flash[:notice].html_safe, :class=>"flash notice")
    end
    message_frame
  end

end
