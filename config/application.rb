require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Kinedu
  class Application < Rails::Application
    config.autoload_paths += %W( #{config.root}/lib )
    
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :es
    
    config.active_record.default_timezone = :local
    config.active_record.time_zone_aware_attributes = false
    
    config.encoding = "utf-8"

    config.active_record.raise_in_transactional_callbacks = true
  end
end
