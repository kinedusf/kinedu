Rails.application.routes.draw do
  
  root :to => "user_sessions#new"

  match 'login' => 'user_sessions#new', :via => [:get], :as => :login
  match 'logout' => "user_sessions#destroy", :via => [:get], :as => :logout

  resources :user_sessions
  resources :users
  resources :activity_logs

  #EndPoints
  namespace :api do
    resources :babies do
      resources :activity_logs
    end
    resources :activities
    resources :activity_logs
  end

end