authorization do
  
  role :guest do
    has_permission_on :user_sessions, :to => :manage
    #Esto se debe de eliminar. Agregar skip dentro de los controller
    has_permission_on :api_babies, :to => :manage
    has_permission_on :api_activities, :to => :manage
    has_permission_on :api_activity_logs, :to => :manage
  end

  role :activity_logs do
    has_permission_on :user_sessions, :to => :manage
    has_permission_on :activity_logs, :to => :manage
  end

end

#Privileges
privileges do
  #Default Manage
  privilege :manage do
    includes :index, :create, :update, :destroy
  end
  
  #Session
  privilege :manage, :user_sessions, :includes => [:new]
  
  #Activity Logs
  privilege :manage, :activity_logs, :includes => [:index]

end