CREATE DATABASE  IF NOT EXISTS `demo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `demo`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: localhost    Database: demo
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,'Actividad de grupo','A aliquet scelerisque per adipiscing proin id a condimentum scelerisque parturient a lobortis a eget condimentum venenatis parturient adipiscing velit dictumst.','2017-04-04 05:03:58','2017-04-04 05:03:58'),(2,'Tomar alimentos','Dis inceptos euismod pharetra nibh pulvinar ad non platea tristique imperdiet parturient vestibulum a nascetur lorem sem.Adipiscing id proin velit.','2017-04-04 05:04:08','2017-04-04 05:04:08'),(3,'Ejercicios de destreza manual','A ad hendrerit phasellus arcu sem consequat parturient inceptos parturient ac facilisi suscipit nec dictum non urna vestibulum posuere eget.','2017-04-04 05:04:18','2017-04-04 05:04:18'),(4,'Lectura','Mi scelerisque semper suspendisse praesent nullam leo habitant arcu fermentum condimentum in vestibulum nam ad ipsum convallis orci mi adipiscing a a.Justo vulputate neque parturient quam.','2017-04-04 05:04:24','2017-04-04 05:04:24'),(5,'Siesta','Tellus a volutpat condimentum nibh habitant a sapien porta parturient maecenas praesent dapibus ullamcorper dictumst eget vitae.','2017-04-04 05:04:32','2017-04-04 05:04:32'),(6,'Dinámica al aire libre','A aliquet scelerisque per adipiscing proin id a condimentum scelerisque parturient a lobortis a eget condimentum venenatis parturient adipiscing velit dictumst.','2017-04-04 05:04:48','2017-04-04 05:04:48'),(7,'Dibujar','Mi blandit mattis himenaeos consequat facilisis eu elit venenatis mattis rhoncus sagittis dapibus aliquet a a feugiat scelerisque lacinia cubilia orci.','2017-04-04 05:05:12','2017-04-04 05:05:12'),(8,'Juego','Lobortis a velit a suspendisse tristique elit metus parturient scelerisque a fermentum vestibulum inceptos cum sociosqu consectetur.','2017-04-04 05:05:22','2017-04-04 05:05:22');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_logs`
--

DROP TABLE IF EXISTS `activity_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `baby_id` int(11) DEFAULT NULL,
  `assistant_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `stop_time` datetime DEFAULT NULL,
  `duration` decimal(10,0) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_logs`
--

LOCK TABLES `activity_logs` WRITE;
/*!40000 ALTER TABLE `activity_logs` DISABLE KEYS */;
INSERT INTO `activity_logs` VALUES (1,1,1,1,'2018-05-18 20:54:04',NULL,NULL,NULL,'2018-05-18 20:54:04','2018-05-18 20:54:04');
/*!40000 ALTER TABLE `activity_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assistants`
--

DROP TABLE IF EXISTS `assistants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assistants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assistants`
--

LOCK TABLES `assistants` WRITE;
/*!40000 ALTER TABLE `assistants` DISABLE KEYS */;
INSERT INTO `assistants` VALUES (1,'Idona Rodgers','2','684-3809 Commodo St.','1-695-273-9436','2014-03-10 07:44:36','2016-04-09 06:53:18'),(2,'Basia Reeves','2','889-3375 Semper Av.','1-288-486-8146','2016-03-08 09:43:53','2016-11-14 06:41:58'),(3,'Charlotte Skinner','3','Ap #711-5535 Pellentesque St.','1-619-558-3983','2016-05-23 12:44:29','2016-04-08 07:34:19'),(4,'Lysandra Solis','4','P.O. Box 330, 663 Imperdiet, Av.','1-545-702-7245','2016-01-21 15:32:11','2016-11-26 11:58:52'),(5,'Samantha Porter','5','325-1746 Vulputate Rd.','1-299-762-7562','2017-03-11 11:13:58','2016-10-11 22:03:39'),(6,'Iliana Dillon','4','3044 Maecenas St.','1-267-433-8360','2018-03-03 09:53:59','2017-01-17 07:43:35'),(7,'Aphrodite Ballard','3','525-3906 Aliquam Av.','1-185-833-0997','2014-07-09 21:18:31','2017-01-19 13:51:37'),(8,'Francesca Baxter','1','331-3950 Cum Ave','1-375-506-2188','2016-01-25 00:09:16','2016-10-25 23:45:04'),(9,'Ria Robles','3','7081 Velit Rd.','1-553-822-3077','2015-01-20 09:36:20','2016-05-10 22:46:56'),(10,'Kay Brewer','1','P.O. Box 406, 1165 Fermentum Ave','1-120-416-8056','2017-10-27 14:41:37','2017-03-19 13:32:20');
/*!40000 ALTER TABLE `assistants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `babies`
--

DROP TABLE IF EXISTS `babies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `babies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `babies`
--

LOCK TABLES `babies` WRITE;
/*!40000 ALTER TABLE `babies` DISABLE KEYS */;
INSERT INTO `babies` VALUES (1,'Merritt','2014-02-10','Winifred Pollard','Ferdinand Gibbs','4480 Egestas. Av.','1-446-909-2123','2016-07-06 20:58:57','2016-10-19 06:33:09'),(2,'Cherokee','2014-08-12','Dahlia Castro','Zeus Huff','311-7263 Cras St.','1-704-365-7793','2015-06-17 11:33:42','2016-04-08 15:23:48'),(3,'Justin','2014-12-15','Miriam Vincent','Edan Jacobs','5466 Luctus Street','1-462-622-9957','2015-07-15 09:19:16','2017-01-12 01:51:52'),(4,'Felicia','2015-09-17','Anjolie Herring','Ali Walker','436-5429 Fermentum Street','1-348-621-8566','2015-12-07 09:42:08','2016-10-19 13:17:19'),(5,'Kylynn','2016-04-14','Heidi Frank','Lee Pacheco','P.O. Box 675, 1364 Quam. St.','1-265-589-1544','2017-11-02 04:02:06','2017-01-28 17:29:29'),(6,'Sigourney','2014-02-04','Piper Mckee','Martin Blackburn','8165 Facilisis Rd.','1-504-381-7639','2017-06-14 07:19:03','2017-02-13 13:07:03'),(7,'Cadman','2015-04-04','Rama Roach','Joseph Hester','P.O. Box 347, 1647 Id Rd.','1-336-831-9779','2015-05-30 07:05:22','2016-07-30 08:33:44'),(8,'Odessa','2014-07-22','Adena Willis','Cody Neal','P.O. Box 175, 6045 Magnis Av.','1-148-457-1049','2014-10-23 18:47:35','2016-08-06 10:32:21'),(9,'May','2015-10-04','Irene Mason','Hayden Wynn','P.O. Box 894, 1672 Vulputate Avenue','1-566-724-6986','2016-05-31 07:16:55','2017-01-22 07:00:29'),(10,'Germaine','2016-06-22','Lacota Drake','Dexter Guerrero','2766 Odio, Ave','1-273-850-9371','2018-01-14 17:30:16','2016-09-07 02:17:54'),(11,'Cameron','2014-01-20','Leslie Campos','Lucas Carney','7906 Sed St.','1-863-613-9498','2015-08-31 12:56:36','2016-04-14 10:17:21'),(12,'Ivan','2016-04-27','Guinevere Mosley','Simon Henderson','287-2764 Massa. Road','1-400-340-0262','2015-01-28 15:12:50','2017-02-21 00:53:17'),(13,'Orlando','2014-04-07','Odessa Foley','Bernard Warner','P.O. Box 110, 8239 Congue Av.','1-145-376-4949','2017-01-04 18:21:43','2016-04-19 00:16:52'),(14,'Florence','2014-11-08','Diana Vargas','Gannon Wolfe','6175 Dictum. Rd.','1-556-206-7899','2017-10-31 09:57:06','2017-01-27 16:37:04'),(15,'Gannon','2015-02-14','Vera Booker','Armando Obrien','P.O. Box 864, 5585 Purus Street','1-349-271-5416','2017-03-19 07:57:07','2016-08-24 19:00:06'),(16,'Quincy','2016-01-15','Heidi Anthony','Evan Figueroa','Ap #528-435 Taciti Av.','1-653-548-0085','2017-08-06 14:15:57','2016-08-18 14:02:00'),(17,'Carter','2014-01-15','Chloe Barlow','Simon Miller','Ap #370-2298 Amet St.','1-516-286-3232','2017-03-22 07:45:31','2016-10-30 21:50:57'),(18,'Aspen','2015-06-30','Althea Avila','Malik Perez','9727 Ligula. St.','1-400-786-8962','2016-01-28 13:15:49','2016-09-22 00:53:58'),(19,'Nicole','2015-05-03','Cameron Stevens','Alan James','2452 Placerat Rd.','1-445-117-1232','2014-02-16 11:20:09','2017-01-13 16:47:14'),(20,'Zephr','2016-03-31','Maxine Riggs','Lev Nieves','466-3407 Taciti Avenue','1-984-120-2087','2014-08-23 18:32:58','2016-06-02 04:08:46'),(21,'Charde','2015-05-31','Raven Velasquez','Fletcher Quinn','650-590 Vivamus Road','1-715-609-6892','2014-05-05 17:08:25','2016-04-09 15:39:38'),(22,'Hall','2014-07-22','Leigh Navarro','Fulton Newman','P.O. Box 791, 6991 Pretium Rd.','1-323-739-4636','2016-05-01 00:37:55','2016-06-27 13:42:45'),(23,'Yvette','2015-03-03','Erin Burnett','Addison Justice','Ap #814-213 Sem Rd.','1-154-870-6393','2018-02-16 00:45:18','2016-12-01 09:52:39'),(24,'Amir','2015-03-24','Jasmine Ramos','Kasper Odom','Ap #128-3848 Nisl. St.','1-579-833-9407','2016-05-06 10:44:47','2017-03-05 01:33:51'),(25,'Jorden','2015-03-05','Bell Rhodes','Lars Fitzgerald','P.O. Box 324, 1229 Varius. Rd.','1-679-808-2463','2014-11-30 16:24:03','2016-11-14 14:28:24'),(26,'Lev','2015-03-26','Phoebe Mason','Herrod Jones','Ap #355-8571 Neque Av.','1-797-545-1206','2018-02-11 15:15:40','2017-02-23 12:28:49'),(27,'Vivian','2015-10-31','Jemima Maxwell','Luke Daugherty','P.O. Box 147, 4733 Duis Rd.','1-526-867-3950','2017-07-26 05:06:44','2016-07-06 13:41:22'),(28,'Tasha','2014-03-18','Dakota Benjamin','Quamar Santos','4224 Ante Road','1-994-862-3874','2015-03-21 03:33:14','2016-06-12 18:16:12'),(29,'Colin','2015-11-10','Urielle West','Rashad Harrington','P.O. Box 740, 2807 A, Street','1-232-490-7613','2016-05-11 03:04:55','2016-04-22 15:43:43'),(30,'Adena','2014-08-11','Jordan Paul','Xanthus Harding','Ap #189-7493 Vestibulum Rd.','1-389-926-0372','2014-02-25 03:00:06','2017-02-11 17:38:22'),(31,'Darryl','2014-11-14','Brianna Wynn','Neil Short','P.O. Box 439, 8440 Pellentesque Road','1-879-306-4328','2016-09-30 00:32:11','2016-06-21 08:34:47'),(32,'Winifred','2014-11-09','Quemby Page','Emerson Small','Ap #122-5523 Aenean Ave','1-146-650-7522','2014-07-24 01:29:57','2016-04-25 07:47:12'),(33,'Elliott','2015-01-16','Cleo Copeland','Hamish Fowler','Ap #928-7351 Arcu. St.','1-963-596-1856','2016-08-22 01:39:12','2016-04-29 01:46:06'),(34,'Ciaran','2016-05-21','Leila Chen','Brennan Thomas','807-5947 Purus. Av.','1-740-517-6021','2016-02-03 02:43:57','2017-01-21 19:15:29'),(35,'Magee','2014-11-07','Dominique Duffy','Howard Haynes','Ap #353-831 Ut St.','1-365-774-4966','2017-03-03 17:00:14','2016-07-04 21:41:07'),(36,'Lacy','2016-01-31','Aimee Robles','Myles Roth','P.O. Box 626, 9177 Tellus, Rd.','1-168-860-1390','2014-01-16 08:45:37','2016-05-14 08:14:34'),(37,'Finn','2014-04-11','Shea Hall','Chancellor Montgomery','354-6074 Convallis Road','1-124-513-1284','2015-11-09 00:37:43','2017-02-10 18:08:29'),(38,'Jael','2015-12-24','Lucy Wright','Valentine Ballard','Ap #231-3805 Ut Av.','1-227-578-9430','2014-05-27 06:29:48','2016-05-23 06:05:31'),(39,'Sean','2014-07-20','Tara Garrett','Tanner Keller','Ap #459-4830 Porttitor Street','1-638-299-4236','2014-01-29 12:29:02','2016-06-12 19:31:30'),(40,'Xyla','2015-05-09','Zia Torres','Salvador Frederick','5999 Odio Rd.','1-137-750-5090','2016-11-10 17:32:52','2016-10-28 10:51:50'),(41,'Noel','2015-11-13','Remedios Chang','John Brewer','675-2112 Donec Rd.','1-244-909-8589','2017-03-11 20:42:24','2016-06-07 21:24:12'),(42,'Xerxes','2014-08-04','Myra Elliott','Howard Cruz','Ap #714-7078 Lorem Av.','1-284-194-9553','2016-06-06 06:37:49','2016-08-26 11:43:25'),(43,'Halla','2015-01-23','Gloria Christian','Caldwell Hayes','Ap #148-6536 Dolor. Rd.','1-516-378-8767','2015-12-29 08:20:18','2016-12-26 02:01:58'),(44,'Driscoll','2014-10-02','Libby Mcmahon','Chadwick Jimenez','Ap #400-9988 A Road','1-678-343-0708','2018-01-10 23:23:59','2016-12-25 20:11:22'),(45,'Ina','2015-09-17','Charity Clark','Grant Dickerson','P.O. Box 228, 1322 Diam. St.','1-155-157-0250','2015-05-10 11:17:48','2016-09-13 12:19:16'),(46,'Ian','2015-03-13','Jeanette Roberts','Victor Cunningham','Ap #574-5831 Phasellus Street','1-701-149-7758','2014-09-04 20:07:40','2017-03-09 19:53:27'),(47,'Jolie','2015-06-24','Gisela Sykes','Oliver Combs','P.O. Box 396, 8260 Adipiscing Avenue','1-980-260-5600','2017-05-10 20:11:09','2016-12-03 18:38:39'),(48,'Tatum','2015-11-12','Lacy Simmons','Lucian Aguilar','998-7953 Neque. Av.','1-225-741-0147','2015-09-27 01:59:28','2016-06-25 01:59:49'),(49,'Jacqueline','2014-11-07','Sophia Hansen','Lucius Huber','Ap #279-6744 Nec, Av.','1-884-116-5934','2014-05-14 09:17:57','2016-04-04 03:55:06'),(50,'Dean','2015-03-25','Haviva Greene','Dylan Madden','793-4590 Lorem Street','1-991-106-9557','2014-09-09 19:27:00','2016-07-24 02:59:33'),(51,'Riley','2014-05-11','Shaeleigh Valentine','Nigel Zamora','3423 Morbi Ave','1-428-201-3626','2016-06-06 23:20:39','2016-07-15 20:23:14'),(52,'Malcolm','2015-08-27','Basia House','Ezekiel Pollard','P.O. Box 829, 8889 Amet St.','1-190-278-5633','2017-02-06 05:16:13','2016-09-25 17:56:58'),(53,'Wyatt','2014-09-13','Rylee Pacheco','Matthew Griffin','1916 Pede. St.','1-507-221-2708','2016-01-20 05:40:54','2016-12-28 17:27:11'),(54,'Phelan','2015-02-24','Pascale Roy','Hyatt Adkins','514-1378 Elit. Road','1-519-444-5292','2016-03-10 09:11:43','2016-10-22 07:06:16'),(55,'Gregory','2014-10-21','Halla Rice','Troy Goff','4508 Bibendum. Rd.','1-667-221-6141','2014-10-06 15:45:38','2016-10-14 09:25:21'),(56,'Samantha','2014-08-17','Keely Tillman','Hashim Foster','P.O. Box 964, 5482 Erat Av.','1-450-871-5402','2018-02-27 19:01:30','2016-07-11 16:10:47'),(57,'Wyatt','2016-04-15','Stephanie Beasley','Edward Peters','Ap #135-6602 Nonummy Avenue','1-901-931-6589','2017-03-16 04:09:28','2016-10-16 10:28:51'),(58,'Elliott','2016-01-10','Kitra Delaney','Hayden Hull','P.O. Box 585, 6215 Ipsum Avenue','1-759-426-9978','2014-05-02 19:11:24','2016-08-16 10:12:33'),(59,'Evangeline','2015-11-22','Karyn Shaw','Victor Farrell','P.O. Box 903, 4790 Libero Av.','1-295-139-0324','2014-12-18 16:22:01','2017-01-31 18:23:50'),(60,'Daryl','2014-04-06','Regina Thornton','Ryder Farrell','P.O. Box 607, 9581 Nam St.','1-169-819-8871','2016-12-14 18:26:58','2017-01-20 09:59:04'),(61,'Troy','2015-02-16','Cheryl Melton','Cooper Kidd','499-6223 Faucibus Av.','1-869-633-8315','2017-08-12 10:11:31','2016-10-11 12:19:59'),(62,'Colin','2014-08-31','Cheryl Moss','Clarke Hunt','5308 Eu St.','1-906-166-2612','2017-03-22 01:37:37','2017-01-25 02:13:47'),(63,'Aubrey','2014-08-25','Medge Suarez','Kadeem Little','1936 Justo. Ave','1-569-212-5858','2017-02-18 12:29:38','2016-08-26 20:20:36'),(64,'Helen','2015-11-04','Meredith Lindsay','Malcolm Gould','Ap #987-709 Integer Rd.','1-241-619-9178','2014-10-16 09:44:52','2016-08-26 18:38:45'),(65,'Wayne','2014-01-10','Brooke Gordon','David Fleming','Ap #555-1644 Proin Rd.','1-869-709-4836','2016-09-21 22:52:44','2017-03-24 17:39:05'),(66,'Sybil','2015-05-27','Hiroko Leach','Joseph Pennington','5770 Elementum St.','1-835-815-3781','2016-12-05 11:29:52','2016-10-18 23:56:23'),(67,'Vincent','2014-03-25','Amaya Harrington','William Langley','P.O. Box 428, 8674 Et St.','1-232-665-4006','2014-05-21 02:49:58','2017-01-30 02:20:27'),(68,'Roth','2016-03-17','Winter Aguilar','Ross Dalton','P.O. Box 174, 651 Mauris Street','1-635-765-8896','2015-04-15 03:30:23','2016-08-16 10:04:23'),(69,'Arthur','2014-08-30','Quintessa Hendrix','Carter Wolfe','Ap #701-1150 Egestas. Rd.','1-263-367-9594','2016-11-30 00:50:03','2017-01-15 09:48:33'),(70,'Leah','2015-10-23','Kellie Vasquez','Dexter Lindsey','Ap #144-7926 Sed St.','1-771-486-9145','2016-09-24 00:00:48','2016-08-04 15:31:15'),(71,'Tad','2015-09-22','Xyla Freeman','Jerome Erickson','P.O. Box 778, 5528 Urna St.','1-187-334-8650','2017-09-16 06:31:20','2016-10-12 13:46:08'),(72,'MacKensie','2015-02-05','Miranda Cross','Victor Singleton','551-372 In Road','1-921-268-0865','2015-09-12 07:20:15','2016-09-01 18:42:04'),(73,'Yuri','2015-09-26','Morgan Russell','Orson Wolf','P.O. Box 293, 3422 Bibendum. Avenue','1-556-418-9640','2016-03-07 10:23:04','2016-10-16 23:35:57'),(74,'Elijah','2014-06-06','Alexandra Hester','Vaughan Robles','553-3004 Mattis Road','1-832-810-0889','2017-05-30 18:41:53','2016-12-12 14:22:15'),(75,'Britanney','2014-06-13','Quincy Barry','Dennis Carson','P.O. Box 987, 5745 Orci St.','1-281-141-5192','2017-12-15 14:21:02','2016-07-04 13:38:23'),(76,'Iola','2014-11-21','Kevyn Aguilar','Drew Mcpherson','P.O. Box 925, 1607 Massa. St.','1-686-306-3308','2015-04-21 19:10:19','2016-05-24 11:58:40'),(77,'Jared','2014-02-01','Ifeoma Pratt','Lars Howard','5440 Ac Rd.','1-505-705-0639','2018-01-28 16:03:04','2016-08-15 01:20:43'),(78,'Nola','2016-04-28','Gisela Rocha','Hayes Morris','2998 Neque Street','1-745-194-7699','2016-04-11 14:39:52','2016-07-03 01:58:53'),(79,'Hollee','2015-02-13','Ria Barlow','August Langley','432-2958 Nulla St.','1-743-957-7608','2015-05-01 07:43:04','2016-06-12 05:55:56'),(80,'Hop','2016-02-21','Serena Britt','Elton Middleton','Ap #998-8543 Phasellus Street','1-102-145-4736','2017-11-23 05:56:32','2016-09-15 12:19:23'),(81,'Jordan','2014-09-18','Claire Vazquez','Conan Harper','Ap #528-9994 Ac Av.','1-533-851-4947','2016-12-26 09:11:19','2017-02-07 10:01:19'),(82,'Linus','2016-06-11','Quintessa Vang','Bevis Whitney','Ap #290-6945 Convallis Road','1-444-594-2484','2014-07-23 15:57:01','2016-07-25 21:02:17'),(83,'Odessa','2014-02-02','Marcia Branch','Tarik Holmes','3867 Dolor Avenue','1-228-976-8591','2014-06-26 21:37:49','2016-09-04 08:04:24'),(84,'Dexter','2014-05-30','Karyn Williamson','Jerome Estes','140-1213 Convallis Ave','1-325-420-8673','2017-11-27 21:10:15','2016-07-13 08:30:09'),(85,'Hector','2014-01-16','Hollee Griffith','Alvin Holden','Ap #926-6711 Semper Street','1-309-782-6022','2016-06-03 06:27:08','2017-03-16 11:57:11'),(86,'Stuart','2014-09-12','Leila David','Richard Fuentes','8765 Neque Road','1-778-321-6612','2015-11-20 21:07:10','2017-03-02 21:06:22'),(87,'Flynn','2015-03-02','Brenda Salazar','Lance Miles','5250 Amet, Road','1-536-913-8074','2017-07-20 18:50:24','2016-04-03 20:51:48'),(88,'Adena','2014-09-30','Christen Hicks','Tarik Lara','P.O. Box 886, 2547 Ac, Rd.','1-936-537-8075','2016-05-06 05:32:13','2017-01-12 04:13:17'),(89,'Hiroko','2014-04-24','Alfreda Sherman','Marsden Caldwell','8700 Magna. Rd.','1-999-291-7603','2014-07-31 03:22:31','2016-09-30 14:35:34'),(90,'Morgan','2014-05-26','Ursa Espinoza','Jameson Kramer','125-5542 Placerat, Av.','1-283-249-3622','2016-05-31 11:23:56','2016-09-28 02:26:56'),(91,'Samuel','2014-09-27','Chastity Meyer','Fletcher Frank','1136 Tristique Ave','1-195-432-1455','2015-02-18 19:14:23','2017-02-20 00:07:24'),(92,'Daniel','2016-01-10','Priscilla Sosa','Lane Rollins','1083 Urna St.','1-659-156-5122','2015-08-19 06:33:31','2017-02-24 23:02:27'),(93,'Kelsey','2014-06-27','Unity Reese','Francis Savage','9578 Fermentum Street','1-982-662-0803','2018-03-08 11:51:15','2017-03-03 01:52:02'),(94,'Zeus','2015-12-04','Melanie Guthrie','Randall Stephenson','Ap #327-2817 Sed Rd.','1-947-853-4041','2018-02-23 00:15:52','2016-11-21 19:14:36'),(95,'Stella','2015-09-29','Cheyenne Yang','Hashim Evans','Ap #614-9047 Integer Rd.','1-770-993-7532','2015-04-17 18:54:57','2017-03-31 10:43:03'),(96,'Zephania','2014-03-06','Cally Salinas','Lawrence Velez','P.O. Box 157, 8556 Libero. Street','1-885-859-7048','2017-05-28 15:52:53','2016-09-08 16:35:42'),(97,'Ethan','2016-04-26','Unity Christian','Graiden Ayala','P.O. Box 351, 5555 Vivamus Road','1-992-116-8149','2014-04-13 05:36:12','2017-04-01 08:51:34'),(98,'Burton','2015-05-22','Leslie Roberson','Lucas Morin','P.O. Box 647, 3210 Mauris St.','1-822-211-6392','2017-09-29 13:42:55','2016-04-05 21:08:27'),(99,'Irma','2015-06-20','Bryar Marks','Vaughan Tyler','2974 Sem, Ave','1-985-581-5602','2016-05-18 23:46:19','2017-03-31 03:08:03'),(100,'Odette','2015-03-11','Isadora Mclaughlin','Preston Greer','Ap #383-1346 Nulla Road','1-587-784-0533','2014-08-24 07:21:03','2016-04-28 02:38:09');
/*!40000 ALTER TABLE `babies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'activity_logs',1,1,'2018-05-18 15:01:50','2018-05-18 15:01:50');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20180517150420'),('20180517154248'),('20180518034221'),('20180518134856'),('20180518141849'),('20180518141932'),('20180518142020');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_sessions`
--

LOCK TABLES `user_sessions` WRITE;
/*!40000 ALTER TABLE `user_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) DEFAULT '1',
  `comments` text,
  `login` varchar(32) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `crypted_password` varchar(255) DEFAULT NULL,
  `password_salt` varchar(255) DEFAULT NULL,
  `persistence_token` varchar(255) DEFAULT NULL,
  `single_access_token` varchar(255) DEFAULT NULL,
  `login_count` int(11) DEFAULT '0',
  `failed_login_count` int(11) DEFAULT '0',
  `last_request_at` datetime DEFAULT NULL,
  `current_login_at` datetime DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `current_login_ip` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `session_key` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `perishable_token` varchar(255) DEFAULT '',
  `verified` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,NULL,'admin','demo@kinedu.com','es','$2a$10$jWFEjt2dcreZI7YlzSo7LuUSIc25NaZUBJj4wM3Aa.qLgtzZOyOyO','HGgaZE9sWrfCQFz98Jd7','526bab4ede373aa3b034d61032d22d4c774353e226c20300719e70032518168471b80c769c52fb558b3cb8eeae30207571ebff8d06a4f3bd328641bf672d4e24','3hkR8Fs5UqAlemhp5UPy',9,0,'2018-05-18 22:09:16','2018-05-18 22:00:05','2018-05-18 21:29:57','::1','::1','54f2046f3a91962e5b14aa87b1ff0d68',NULL,NULL,'CAye2c0MVZITjD55pXAO',0,'2018-05-17 18:38:24','2018-05-18 22:09:16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 22:13:46
